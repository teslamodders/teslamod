﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeslaMod
{
    public interface TeslaModInterface
    {
        string GetName();
        void OnEnable();
        void OnReload();
        void OnDisable();
    }
   

}
