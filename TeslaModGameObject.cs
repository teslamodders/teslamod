﻿using UnityEngine;
using TeslaLib;
using System.Collections.Generic;

namespace TeslaMod
{
    class TeslaModGameObject : MonoBehaviour
    {
        public TeslaModLoader loader;
        public ModManager modManager;

        private GUIStyle style;
        private bool menuDisplayed = false;

        public void Start()
        {
            style = new GUIStyle();
            style.font = FontFetcher.GetFont(TeslagradFont.Constantine);
        }


        public void OnGUI()
        {
            if (modManager != null && loader != null)
            {
                if (GUI.Button(new Rect(1500, 50, 200, 50), "TeslaMods"))
                {
                    menuDisplayed = !menuDisplayed;
                }

                if (menuDisplayed)
                {
                    int pos = 0;
                    foreach (string modName in loader.availableMods)
                    {
                        if (GUI.Toggle(new Rect(1500, 100 + pos, 200, 30), modManager.enabledMods.ContainsKey(modName), modName))
                        {
                            if (!modManager.enabledMods.ContainsKey(modName))
                            {
                                modManager.EnableMod(modName);
                            }
                        }
                        else
                        {
                            if (modManager.enabledMods.ContainsKey(modName))
                            {
                                modManager.DisableMod(modName);
                            }
                        }
                        pos += 30;
                    }
                }
            }
        }
    }
}
