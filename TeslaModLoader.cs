﻿using System.Collections.Generic;
using UnityEngine;

namespace TeslaMod
{
    public class TeslaModLoader
    {
        public GameObject go;
        public List<string> availableMods = new List<string>();
        public static ModManager modManager;

        public void OnGameStart()
        {
            if (modManager == null)
            {
                modManager = new ModManager();
                OnScreenDebug.Log("TeslaMod successfully loaded !");
                availableMods.Add("TeslaLibIntegration");
                availableMods.Add("EighteenXX");
                availableMods.Add("BlinkFast");
                availableMods.Add("TwitchChat");
                availableMods.Add("TwitchGrad");
            }
            else
            {
                modManager.ReloadAllMods();
            }

            go = new GameObject();
            TeslaModGameObject teslamodgo = go.AddComponent<TeslaModGameObject>();
            teslamodgo.loader = this;
            teslamodgo.modManager = modManager;

            
        }
    }
}
