﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace TeslaMod
{
    public class ModManager
    {
        public Dictionary<string, TeslaModInterface> enabledMods = new Dictionary<string, TeslaModInterface>();

        public void EnableMod(string name)
        {
            var dll = Assembly.LoadFile(Application.dataPath + "/Managed/" + name + ".dll");
            Type modType = dll.GetType(name+"."+name);

            TeslaModInterface mod = Activator.CreateInstance(modType) as TeslaModInterface;
            if (mod != null)
            {
                mod.OnEnable();
                enabledMods.Add(name, mod);
            }
        }

        public void ReloadAllMods()
        {
            foreach (KeyValuePair<string, TeslaModInterface> kvp in enabledMods)
            {
                kvp.Value.OnReload();
            }
        }

        public void DisableMod(string name)
        {
            enabledMods[name].OnDisable();
            enabledMods.Remove(name);
        }

        public TeslaModInterface GetMod(string name)
        {
            if (enabledMods.ContainsKey(name))
            {
                return enabledMods[name];
            }

            return null;
        }
    }
}
